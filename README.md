# Naxos Fakes

This repo contains my nixified dotfiles done in a literal programming style.

You can read the compiled [html version](https://stratosgear.gitlab.com//naxos-fakes).

Or you might just be interested in the "dry" actual [nix and
dotfiles](https://gitlab.com/stratosgear/naxos-fakes/-/tree/master/nix?ref_type=heads)
