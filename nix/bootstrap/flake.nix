# ~/~ begin <<docs/bootstrap/flake.md#nix/bootstrap/flake.nix>>[init]
{
  description = "Bootstraping, aka installing, Nixos on new hosts";

  nixConfig = {
    experimental-features = [ "nix-command" "flakes" ];
    substituters = [
      "http://192.168.3.91:8282"
      "https://cache.nixos.org/"
    ];

    extra-substituters = [
      # Nix community's cache server
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = { self, nixpkgs, disko, ... } @ inputs:
    let
      inherit (self) outputs;
      lib = nixpkgs.lib;
      systems = [ "x86_64-linux" ];
      forEachSystem = f: lib.genAttrs systems (sys: f pkgsFor.${sys});
      pkgsFor = nixpkgs.legacyPackages;
    in
    {

      inherit lib;

      formatter = forEachSystem (pkgs: pkgs.nixpkgs-fmt);

      nixosConfigurations = {

        # ~/~ begin <<docs/hosts/ion/system/index.md#ion-bootstrap>>[init]
        ion = lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            disko.nixosModules.disko
            ../hosts/ion/hardware-configuration.nix
            ../hosts/ion/configuration.nix
          ];
          specialArgs = { inherit inputs outputs; };
        };
        # ~/~ end
        # ~/~ begin <<docs/hosts/nixos2/system/index.md#nixos2-bootstrap>>[init]
        nixos2 = lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            disko.nixosModules.disko
            ../hosts/nixos2/configuration.nix
          ];
          specialArgs = { inherit inputs outputs; };
        };
        # ~/~ end
        # ~/~ begin <<docs/hosts/vezzini/system/index.md#vezzini-bootstrap>>[init]
        vezzini = lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            disko.nixosModules.disko
            ../hosts/vezzini/hardware-configuration.nix
            ../hosts/vezzini/configuration.nix
          ];
          specialArgs = { inherit inputs outputs; };
        };
        # ~/~ end

      };
    };
}
# ~/~ end