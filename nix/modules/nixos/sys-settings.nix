# ~/~ begin <<docs/system/settings.md#nix/modules/nixos/sys-settings.nix>>[init]
{ lib, pkgs, ... }:
let
  inherit (lib) types mkOption;
in
{
  options.sys.settings = {

    stratosSshKey = mkOption {
      type = types.str;
      description = "The public ssh key that Stratos usually uses";
      default = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGnvOdbpfS51yVj1XiFuglaWlUxt5brl1/BfufaYHahm stratos@iocaine";
    };

    default = {
      shell = mkOption {
        type = types.nullOr (types.enum [ "${pkgs.fish}/bin/fish" "${pkgs.zsh}/bin/zsh" ]);
        description = "The default shell to use";
        default = "${pkgs.fish}/bin/zsh";
      };

      terminal = mkOption {
        type = types.nullOr (types.enum [ "alacritty" "${pkgs.foot}/bin/foot" "kitty" ]);
        description = "The default terminal to use";
        default = "kitty";
      };

      browser = mkOption {
        type = types.nullOr (types.enum [ "firefox" ]);
        description = "The default browser to use";
        default = "firefox";
      };

      editor = mkOption {
        type = types.nullOr (types.enum [ "nvim" "code" "micro" ]);
        description = "The default editor to use";
        default = "micro";
      };
    };

    impermanenceEnabled = mkOption {
      type = types.bool;
      description = "Whether to enable impermanence to delete home directory on reboot";
      default = false;
    };

    wallpaper = mkOption {
      type = types.str;
      default = "";
      description = ''
        Wallpaper path
      '';
    };

    fonts = {
      regular = mkOption {
        type = types.str;
        description = "The font for regular text";
        default = "Fira Sans";
      };

      monospace = mkOption {
        type = types.str;
        description = "The font for monospace text";
        default = "MonoLisa Nerd Font";
      };
    };

    monitor1 = mkOption {
      type = types.str;
      description = "The first monitor";
      default = "";
    };

    monitor2 = mkOption {
      type = types.nullOr (types.str);
      description = "The second monitor, if any";
      default = null;
    };

    monitor3 = mkOption {
      type = types.nullOr (types.str);
      description = "The third monitor, if any";
      default = null;
    };

    hostname = mkOption {
      type = types.str;
      description = "Hostname";
      default = "someHostname";
    };

  };
}
# ~/~ end