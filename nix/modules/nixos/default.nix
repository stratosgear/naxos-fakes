# ~/~ begin <<docs/system/settings.md#nix/modules/nixos/default.nix>>[init]
# Add your reusable NixOS modules to this directory, on their own file (https://nixos.wiki/wiki/Module).
# These should be stuff you would like to share with others, not your personal configurations.
{
  # List your module files here
  settings = import ./sys-settings.nix;
}
# ~/~ end