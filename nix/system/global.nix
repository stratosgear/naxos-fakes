# ~/~ begin <<docs/system/packages/index.md#nix/system/global.nix>>[init]
{ inputs, outputs, pkgs, lib, ... }: {

  imports = [
    inputs.home-manager.nixosModules.home-manager
    ./sops.nix
    ./nix.nix
    # ./optin-persistence.nix
    # ./tailscale.nix
  ] ++ (builtins.attrValues outputs.nixosModules);

  home-manager.extraSpecialArgs = { inherit inputs outputs; };

  # Basic global installation of programs that do not need additional configuration
  environment.systemPackages = [
    pkgs.sops
    pkgs.nixpkgs-fmt
  ];

  # ~/~ begin <<docs/system/i18n.md#global-i18n>>[init]
  i18n = {
    defaultLocale = lib.mkDefault "en_US.UTF-8";
    extraLocaleSettings = {
      LC_TIME = lib.mkDefault "es_ES.UTF-8";
    };
    supportedLocales = lib.mkDefault [
      "en_US.UTF-8/UTF-8"
      "es_ES.UTF-8/UTF-8"
    ];
  };
  time.timeZone = lib.mkDefault "Europe/Madrid";
  # ~/~ end

  nixpkgs = {
    overlays = builtins.attrValues outputs.overlays;
    config = {
      allowUnfree = true;
    };
  };

  # Increase open file limit for sudoers
  security.pam.loginLimits = [
    {
      domain = "@wheel";
      item = "nofile";
      type = "soft";
      value = "524288";
    }
    {
      domain = "@wheel";
      item = "nofile";
      type = "hard";
      value = "1048576";
    }
  ];

  # ~/~ begin <<docs/system/packages/openssh.md#global-openssh>>[init]
  let
    inherit (config.networking) hostName;
    hosts = outputs.nixosConfigurations;
    # TODO: This prepopulates some known ssh hosts. But turning it  off for now
    # pubKey = host: ../../${host}/ssh_host_ed25519_key.pub;

    # Sops needs access to the keys before the persist dirs are even mounted; so
    # just persisting the keys won't work, we must point at /persist
    # hasOptinPersistence = config.environment.persistence ? "/persist";
  in
  {
    services.openssh = {
      enable = true;
      settings = {
        # Harden
        PasswordAuthentication = true;
        # TODO: Turn this OFF after debugging
        PermitRootLogin = "yes";
        # Automatically remove stale sockets
        StreamLocalBindUnlink = "yes";
        # Allow forwarding ports to everywhere
        GatewayPorts = "clientspecified";
      };

      # hostKeys = [{
      #   path = "${lib.optionalString hasOptinPersistence "/persist"}/etc/ssh/ssh_host_ed25519_key";
      #   type = "ed25519";
      # }];
    };

    programs.ssh = {
      # Each hosts public key
      # TODO: This prepopulates some known ssh hosts. But turning it  off for now
      # knownHosts = builtins.mapAttrs
      #   (name: _: {
      #     publicKeyFile = pubKey name;
      #     extraHostNames =
      #       (lib.optional (name == hostName) "localhost");
      #     #   (lib.optional (name == hostName) "localhost") ++ # Alias for localhost if it's the same host
      #     #   (lib.optionals (name == gitHost) [ "m7.rs" "git.m7.rs" ]); # Alias for m7.rs and git.m7.rs if it's the git host
      #   })
      #   hosts;
    };

    # Passwordless sudo when SSH'ing with keys
    security.pam.enableSSHAgentAuth = true;
  }
  # ~/~ end

}
# ~/~ end