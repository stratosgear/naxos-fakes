# ~/~ begin <<docs/hosts/nixos2/system/index.md#nix/hosts/nixos2/configuration.nix>>[init]
{ modulesPath, lib, pkgs, inputs, disko, ... }:
let
  mySshKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGnvOdbpfS51yVj1XiFuglaWlUxt5brl1/BfufaYHahm stratos@iocaine";
  hostname = "nixos2";
in
{
  imports = [
    ./hardware-configuration.nix
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    ./disk-config.nix
  ];


  boot.loader.grub = {
    # no need to set devices, disko will add all devices that have a EF02 partition to the list already
    # devices = [ ];
    efiSupport = true;
    efiInstallAsRemovable = true;
  };

  # ~/~ begin <<docs/bootstrap/flake.md#bootstrap-common-packages>>[init]
    services.openssh = {
      enable = true;
      settings = {
        PermitRootLogin = lib.mkDefault "yes";
      };
    };

    environment.systemPackages = map lib.lowPrio [
      pkgs.curl
      pkgs.gitMinimal
    ];
  # ~/~ end

  # ~/~ begin <<docs/hosts/nixos2/system/index.md#nixos2-users>>[init]
  users = {
    mutableUsers = false;
    users = {

      root = {
        openssh.authorizedKeys.keys = [
          mySshKey
        ];
        hashedPassword = "$6$nZaKtRdcV6EamfPG$JrSN363Yf7T4o6gE9gF8XTB.BoDLpmza05blgRnR.KGKisE9eUFBlrHb3SGB4kPyfs/IYS7cE7JPKHSSmQAnL/";
      };
      stratos = {
        isNormalUser = true;
        extraGroups = [
          "wheel"
        ];
        openssh.authorizedKeys.keys = [
          mySshKey
        ];
        hashedPassword = "$6$fth8aNS.CWpcGJkD$eKQI2wc3n7zib6Rpw50sQpILRkkJNtaDLJoW/zHwfMMappd3rYZNdagO5lRFuld0XgHL7wgJcgpFaP.abO9Wf/";
      };

    };
  };
  # ~/~ end

  networking.hostName = hostname;

  system.stateVersion = "23.11";
}
# ~/~ end