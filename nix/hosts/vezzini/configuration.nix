# ~/~ begin <<docs/hosts/vezzini/system/index.md#nix/hosts/vezzini/configuration.nix>>[init]
{ modulesPath, lib, pkgs, inputs, disko, ... }:
let
  mySshKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGnvOdbpfS51yVj1XiFuglaWlUxt5brl1/BfufaYHahm stratos@iocaine";
  hostname = "vezzini";
  stateVersion = "23.11";
in
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    ./disk-config.nix
  ];

  # Make sure we are formatting the correct disk
  # TODO: Find how to override this. This invocation fails!
  #disko.devices.disk.disk1.device = lib.mkOverride "/dev/nvme0n1";

  boot.loader.grub = {
    # no need to set devices, disko will add all devices that have a EF02 partition to the list already
    # devices = [ ];
    efiSupport = true;
    efiInstallAsRemovable = true;
  };

  # ~/~ begin <<docs/bootstrap/flake.md#bootstrap-common-packages>>[init]
    services.openssh = {
      enable = true;
      settings = {
        PermitRootLogin = lib.mkDefault "yes";
      };
    };

    environment.systemPackages = map lib.lowPrio [
      pkgs.curl
      pkgs.gitMinimal
    ];
  # ~/~ end

  # ~/~ begin <<docs/hosts/vezzini/system/index.md#vezzini-users>>[init]
  users = {
    mutableUsers = false;
    users = {

      root = {
        openssh.authorizedKeys.keys = [
          mySshKey
        ];
        hashedPassword = "$6$nZaKtRdcV6EamfPG$JrSN363Yf7T4o6gE9gF8XTB.BoDLpmza05blgRnR.KGKisE9eUFBlrHb3SGB4kPyfs/IYS7cE7JPKHSSmQAnL/";
      };
      stratos = {
        isNormalUser = true;
        extraGroups = [
          "wheel"
        ];
        openssh.authorizedKeys.keys = [
          mySshKey
        ];
        hashedPassword = "$6$fth8aNS.CWpcGJkD$eKQI2wc3n7zib6Rpw50sQpILRkkJNtaDLJoW/zHwfMMappd3rYZNdagO5lRFuld0XgHL7wgJcgpFaP.abO9Wf/";
      };

    };
  };
  # ~/~ end

  networking.hostName = hostname;

  # ~/~ begin <<docs/hosts/vezzini/system/index.md#vezzini-wifi>>[init]
  # networking.wireless = {
  #   enable = true;
  #   userControlled.enable = true;
  #   networks = {
  #     Vega = {
  #       pskRaw = "3b650bb41bb1036e4c13fbd1a4395fc26a613b2f9d9f9b2079d7d72937634cc9";
  #     };
  #     Azuland = {
  #       pskRaw = "bca7df5a0e951ce37486cf68801092f8cd1563a5809d2ecfc3959ff0e12df4bc";
  #     };
  #   };
  # };
  # ~/~ end

  system.stateVersion = stateVersion;
}
# ~/~ end