# ~/~ begin <<docs/hosts/ion/system/index.md#nix/hosts/ion/configuration.nix>>[init]
{ modulesPath, lib, pkgs, inputs, disko, ... }:
let
  mySshKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGnvOdbpfS51yVj1XiFuglaWlUxt5brl1/BfufaYHahm stratos@iocaine";
  hostname = "ion";
  stateVersion = "23.11";
in
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    ./disk-config.nix
  ];

  # Make sure we are formatting the correct disk
  # TODO: Find how to override this. This invocation fails!
  #disko.devices.disk.disk1.device = lib.mkOverride "/dev/nvme0n1";

  boot.loader.grub = {
    # no need to set devices, disko will add all devices that have a EF02 partition to the list already
    # devices = [ ];
    efiSupport = true;
    efiInstallAsRemovable = true;
  };

  # ~/~ begin <<docs/bootstrap/flake.md#bootstrap-common-packages>>[init]
    services.openssh = {
      enable = true;
      settings = {
        PermitRootLogin = lib.mkDefault "yes";
      };
    };

    environment.systemPackages = map lib.lowPrio [
      pkgs.curl
      pkgs.gitMinimal
    ];
  # ~/~ end

  # ~/~ begin <<docs/hosts/ion/system/index.md#ion-users>>[init]
  users = {
    mutableUsers = false;
    users = {

      root = {
        openssh.authorizedKeys.keys = [
          mySshKey
        ];
        hashedPassword = "$6$nZaKtRdcV6EamfPG$JrSN363Yf7T4o6gE9gF8XTB.BoDLpmza05blgRnR.KGKisE9eUFBlrHb3SGB4kPyfs/IYS7cE7JPKHSSmQAnL/";
      };
      stratos = {
        isNormalUser = true;
        extraGroups = [
          "wheel"
        ];
        openssh.authorizedKeys.keys = [
          mySshKey
        ];
        hashedPassword = "$6$fth8aNS.CWpcGJkD$eKQI2wc3n7zib6Rpw50sQpILRkkJNtaDLJoW/zHwfMMappd3rYZNdagO5lRFuld0XgHL7wgJcgpFaP.abO9Wf/";
      };

    };
  };
  # ~/~ end

  networking.hostName = hostname;

  system.stateVersion = stateVersion;
}
# ~/~ end