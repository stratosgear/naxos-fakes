# Hosts

This page contains the bootstraping info of each host.  They are mainly the same, but we
keep the mseparate in case we ever want to make any exceptions.


## Bootstrap nixos

nixos is a Virtualbox test vm

``` {.nix #nixos-bootstrap}
ion = nixpkgs.lib.nixosSystem {
  system = "x86_64-linux";
  modules = [
    disko.nixosModules.disko
    ../hosts/ion/configuration.nix
  ];
};
```

