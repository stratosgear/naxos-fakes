# Bootstrap Flake

This is the contents of the bootstrap flake (one of the two flakes used in this repo,
the other being the [system flake](../system/flake.md))

## Flake core structure

``` {.nix file=nix/bootstrap/flake.nix}
{
  description = "Bootstraping, aka installing, Nixos on new hosts";

  nixConfig = {
    experimental-features = [ "nix-command" "flakes" ];
    substituters = [
      "http://192.168.3.91:8282"
      "https://cache.nixos.org/"
    ];

    extra-substituters = [
      # Nix community's cache server
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = { self, nixpkgs, disko, ... } @ inputs:
    let
      inherit (self) outputs;
      lib = nixpkgs.lib;
      systems = [ "x86_64-linux" ];
      forEachSystem = f: lib.genAttrs systems (sys: f pkgsFor.${sys});
      pkgsFor = nixpkgs.legacyPackages;
    in
    {

      inherit lib;

      formatter = forEachSystem (pkgs: pkgs.nixpkgs-fmt);

      nixosConfigurations = {

        <<ion-bootstrap>>
        <<nixos2-bootstrap>>
        <<vezzini-bootstrap>>

      };
    };
}
```

## What is installed with the bootstrap flake

Only a very basic set of packages, enough so that you will have a system up and running
and able to continue configuring it.

List of installed packages:

``` {.nix #bootstrap-common-packages}
  services.openssh = {
    enable = true;
    settings = {
      PermitRootLogin = lib.mkDefault "yes";
    };
  };

  environment.systemPackages = map lib.lowPrio [
    pkgs.curl
    pkgs.gitMinimal
  ];
```
## Separate flake?

Is there indeed a need for a separate flake for bootstraping and a different for normal
every day maintenance?

Well, according to my level of nix expertise I could not keep a single flake for both
purposes.  My major issue was the inability to keep sops secrets (and their key
management) separate from each other.  That is why the bootstraping does only the very
basic stuff, and leaves more complex operations, to the maintenance cycle.

