# Bootstrapping

What is bootstrap (in this context)?

I consider **bootstraping** the process by which a new machine (server, laptop, vm etc)
is cleanly installed with Nixos and a bare basic environment is installed.


## 0. Prepare for remote connections

On the host to bootstrap, boot from the standard nixos iso installer.

Set a root password so you can remotely connect to it

```
[nixos@nixos:~]$ sudo su

[root@nixos:/home/nixos]# passwd
New password:
Retype new password:
passwd: password updated succesfully
```

Remember what you typed!

And get the currect IP of the host:

```
[root@nixos:/home/nixos]# ip a
```

If the host does not have an ethernet card, you will **not** have an IP at this time.

To connect through wifi use `nmtui` to make a connection

Now get your IP (might take some time till DHCP assigns you one)

Use these info to create, locally, an ~/.ssh/config section with a host name [hostname]
that allows you to remotely connect to that host. Use user `nixos` for now, later you can
change it to another user.

Also run a `ssh-copy-id -i ~/.ssh/id_ed25519.pub [hostname]` so you can connect
passwordless.


## 1. hardware-configuration.nix

On the remote host, generate a hardware-configuration.nix, with:

`nixos-generate-config --no-filesystems --root /mnt`

Will be generated in /mnt/nixos/hardware-configuration.nix

It should need minimal to **no** editing.

Copy the contents of this file to your local machine in the repo where you keep your
nixos config files, under the folder of `hosts/[hostname]/hardware-configuration.nix`

Also check, `/mnt/nixos/configuration.nix` and note the `system.stateVersion` value.
You will have to use that value in the `hosts/[hostname]/configuration.nix` file too!

### 2. disk-config.nix

On local machine, copy an existing disk-config.nix from another host, or copy and edit
if you need to make specific host disk changes.

### 3. configuration.nix

On local machine, copy an existing configuration.nix from another host, and edit it to
make host specific changes. This file is kept really minimal, enough to be able to
bootstrap the host only.  Further configurations and customizations, will happen in a
later step.

Make sure you use the `system.stateVersion` value you found on step #0.

### 4. Edit bootstrap/flake.nix

Edit bootstrap/flake.nix and add another section to the [nixosConfigurations] sections,
with the new host.

### 5. Start the installation process

From you local Nixos machine, and from the /bootstrap folder, run:

`nix run github:nix-community/nixos-anywhere -- --flake .#[hostname] [hostname]`

This should run cleanly and end with a message of `Installation finished. No error
reported.` somewhere at the end of the log.

Your host is now setup and ready to connect to with a `ssh [hostname]`


## Further host configurations


### Create/update additional.nix

Locally, in the main flake.nix, create a new section in `nixosConfigurations` by copying
and pasting another section from another host.  Name it according to your [hostname]

You notice an additional configuration file, unimaginatively named `additional.nix`. In
this file you are now allowed to proceed with any further costumizations.  Commit them
to your repo.


### Get ssh keys from newly created host and reencrypt secrets

Login to the newly installed host:

`ssh [hostname]`

Extract an age formatted fingerprint of the host ssh keys:

`nix-shell -p ssh-to-age --run 'cat /etc/ssh/ssh_host_ed25519_key.pub | ssh-to-age'``

You want the output string that starts with: `ageXXXXXXX`

Locally, edit the `.sops.yaml` file and at the top add a new `&system_[hostname]` entry
with the age key you extracted above.

Further below, add that new `&system_[hostname]` to all the secrets you want the host to
have access to.

Locally, open a terminal with sops installed, in order to reencrypt the secrets:
nix-shell -p sops

and run:
sops updatekeys file/path/to/secrets.yml

Commit your changes!


# Apply further customizations

Once ready, connect to your remote host, checkout your nix repo and from the root of the
repo run:

sudo nixos-rebuild switch --flake .#[hostname]


## Bootstraping

Create flake.lock:
nix flake lock

Install remotely:
nix run github:nix-community/nixos-anywhere -- --flake ./bootstrap#ion nion

Also, pass the current host ssh keys to remote server:
nix run github:nix-community/nixos-anywhere -- --copy-host-keys --flake .#ion nixos@ion

