# git

## Installation

``` {.nix #global-git}
programs.git = {
  enable = true;
  <<global-git-config>>
};
```

## Configuration

### Basic authoring info

``` {.nix #global-git-config}
userName = "Stratos Gerakakis";
userEmail = "stratos@gerakakis.net";
```


### Aliases

```
aliases = {
  al = "config - -global - l";
  br = "branch";
  co = "checkout";
  df = "difftool - -dir-diff - -tool=meld";
};
```
