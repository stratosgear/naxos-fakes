# vezzini

`vezzini` is a Framework laptop


## Custom Settings

Host specific settings.  We are not setting **all** settings, as some have sane
defaults. We only overwrite what we need.  For the full list of settings see the
[options module definition](../../system/settings.md)

``` {.nix file=nix/hosts/vezzini/settings.nix}
{ pkgs, inputs, config, ... }: {

  config = {
    sys.settings = {
      monitor1 = "virtual1";
      monitor2 = "";
    };
  };

}
```
