# ion

`ion` is tiny Intel Dual Core Atom 330 [ASRock Ion box](https://www.asrock.com/nettop/NVIDIA/ion%20330/)


## Custom Settings

Host specific settings.  We are not setting **all** settings, as some have sane
defaults. We only overwrite what we need.  For the full list of settings see the
[options module definition](../../system/settings.md)

``` {.nix file=nix/hosts/ion/settings.nix}
{ pkgs, inputs, config, ... }: {

  config = {
    sys.settings = {
      monitor1 = "virtual1";
      monitor2 = "";
    };
  };

}
```
