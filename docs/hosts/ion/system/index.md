# ion System info

## Bootstrap

Used during bootstraping only

``` {.nix #ion-bootstrap}
ion = lib.nixosSystem {
  system = "x86_64-linux";
  modules = [
    disko.nixosModules.disko
    ../hosts/ion/hardware-configuration.nix
    ../hosts/ion/configuration.nix
  ];
  specialArgs = { inherit inputs outputs; };
};
```

## Hardware configuration

Extracted from the host during OS installation (with minimum to no editing):

``` {.nix file=nix/hosts/ion/hardware-configuration.nix}
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "ohci_pci" "ehci_pci" "ahci" "usb_storage" "usbhid" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp0s10.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
```


## Basic Configuration

Used only for bootstraping the host. Additional and more complex customizations take
place in [Customization](./customization.md)

``` {.nix file=nix/hosts/ion/configuration.nix}
{ modulesPath, lib, pkgs, inputs, disko, ... }:
let
  mySshKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGnvOdbpfS51yVj1XiFuglaWlUxt5brl1/BfufaYHahm stratos@iocaine";
  hostname = "ion";
  stateVersion = "23.11";
in
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    ./disk-config.nix
  ];

  # Make sure we are formatting the correct disk
  # TODO: Find how to override this. This invocation fails!
  #disko.devices.disk.disk1.device = lib.mkOverride "/dev/nvme0n1";

  boot.loader.grub = {
    # no need to set devices, disko will add all devices that have a EF02 partition to the list already
    # devices = [ ];
    efiSupport = true;
    efiInstallAsRemovable = true;
  };

  <<bootstrap-common-packages>>

  <<ion-users>>

  networking.hostName = hostname;

  system.stateVersion = stateVersion;
}
```


## Disk Partitioning

Defined here and applied automatically with disko:

``` {.nix file=nix/hosts/ion/disk-config.nix}
{ lib, ... }:
{
  disko.devices = {
    disk = {
      disk1 = {
        type = "disk";
        device = lib.mkDefault "/dev/sda";
        content = {
          type = "gpt";
          partitions = {
            boot = {
              name = "boot";
              size = "1M";
              type = "EF02";
            };
            esp = {
              name = "ESP";
              size = "500M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            root = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = [ "-f" ]; # Override existing partition
                # Subvolumes must set a mountpoint in order to be mounted,
                # unless their parent is mounted
                subvolumes = {
                  # Subvolume name is different from mountpoint
                  "/rootfs" = {
                    mountpoint = "/";
                  };
                  # Subvolume name is the same as the mountpoint
                  "/home" = {
                    mountOptions = [ "compress=zstd" ];
                    mountpoint = "/home";
                  };
                  # Sub(sub)volume doesn't need a mountpoint as its parent is mounted
                  "/home/user" = { };
                  # Parent is not mounted so the mountpoint must be set
                  "/nix" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/nix";
                  };
                  # Subvolume for the swapfile
                  "/swap" = {
                    mountpoint = "/.swapvol";
                    swap = {
                      swapfile.size = "20M";
                      swapfile2.size = "20M";
                      swapfile2.path = "rel-path";
                    };
                  };
                };

                mountpoint = "/partition-root";
                swap = {
                  swapfile = {
                    size = "20M";
                  };
                  swapfile1 = {
                    size = "20M";
                  };
                };
              };
            };
          };
        };
      };
    };
  };
}
```

## Users

``` {.nix #ion-users}
users = {
  mutableUsers = false;
  users = {

    root = {
      openssh.authorizedKeys.keys = [
        mySshKey
      ];
      hashedPassword = "$6$nZaKtRdcV6EamfPG$JrSN363Yf7T4o6gE9gF8XTB.BoDLpmza05blgRnR.KGKisE9eUFBlrHb3SGB4kPyfs/IYS7cE7JPKHSSmQAnL/";
    };
    stratos = {
      isNormalUser = true;
      extraGroups = [
        "wheel"
      ];
      openssh.authorizedKeys.keys = [
        mySshKey
      ];
      hashedPassword = "$6$fth8aNS.CWpcGJkD$eKQI2wc3n7zib6Rpw50sQpILRkkJNtaDLJoW/zHwfMMappd3rYZNdagO5lRFuld0XgHL7wgJcgpFaP.abO9Wf/";
    };

  };
};
```
