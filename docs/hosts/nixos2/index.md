# nixos2

`nixos2` is a test Virtualbox VM.  Most of the repo testing/development was done on
`nixos1` and `nixos2` VMs


## Custom Settings

Host specific settings.  We are not setting **all** settings, as some have sane
defaults. We only overwrite what we need.  For the full list of settings see the [module
definition](../../system/settings.md)

``` {.nix file=nix/hosts/nixos2/settings.nix}
{ pkgs, inputs, config, ... }: {

  config = {
    sys.settings = {
      monitor1 = "virtual1";
      monitor2 = "";
    };
  };

}
```
