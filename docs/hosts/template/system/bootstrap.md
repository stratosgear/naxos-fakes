## Bootstrap [host]

Required for bootstraping this host:

[Add {.nix #bootstrap-[host]} to the following code block, when ready]

```
nixosConfigurations.[host] = lib.nixosSystem {
  system = "x86_64-linux";
  modules = [
    disko.nixosModules.disko
    ../hosts/[host]/configuration.nix
  ];
  specialArgs = { inherit inputs outputs; };
};
```
