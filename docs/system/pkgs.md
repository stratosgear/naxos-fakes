# Packages

Creating this, although I believe it might not be needed. (Actually, with the current
setup it **is** needed)

``` {.nix file=nix/pkgs/default.nix}
# Custom packages, that can be defined similarly to ones from nixpkgs
# You can build them using 'nix build .#example'
pkgs: {
  # example = pkgs.callPackage ./example { };
}
```
