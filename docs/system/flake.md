# System Flake

This is the main flake that is used for the maintenance of the system (one of the two
flakes used in this repo, the other being the [bootstrap flake](../bootstrap/flake.md))



``` {.nix file=nix/flake.nix}
{
  description = "Stratosgear master Nixos configurations";

  nixConfig = {
    # TODO: This brings up, up to 6 separate confirmation prompts to activate this
    # setting. Can they be turned off?
    experimental-features = [ "nix-command" "flakes" ];

    substituters = [
      "http://192.168.3.91:8282"
      "https://cache.nixos.org/"
    ];

    extra-substituters = [
      # Nix community's cache server
      "https://nix-community.cachix.org"
    ];

    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    # Nixpkgs
    #nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";

    # You can access packages and modules from different nixpkgs revs
    # at the same time. Here's an working example:
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    # Also see the 'unstable-packages' overlay at 'overlays/default.nix'.

    impermanence.url = "github:nix-community/impermanence";
    # nix-colors.url = "github:misterio77/nix-colors";

    disko.url = "github:nix-community/disko";

    sops-nix = {
      url = "github:mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";

      # This makes homemanager follow the version of nixpkgs, so they stay in sync.
      inputs.nixpkgs-stable.follows = "nixpkgs";
    };

    # Home manager
    home-manager = {
      # Using master here, instead of release-23.05, because we are using HM along nixos 23.05
      # See bug: https://github.com/ryan4yin/nixos-and-flakes-book/issues/49
      # and: https://github.com/nix-community/home-manager/issues/4483
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = { self, nixpkgs, home-manager, disko, ... } @ inputs:
    let
      inherit (self) outputs;
      lib = nixpkgs.lib // home-manager.lib;
      systems = [ "x86_64-linux" ];
      forEachSystem = f: lib.genAttrs systems (system: f pkgsFor.${system});
      pkgsFor = lib.genAttrs systems (system: import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      });
    in
    {

      inherit lib;
      nixosModules = import ./modules/nixos;
      homeManagerModules = import ./modules/nixos;
      # templates = import ./templates;

      overlays = import ./overlays { inherit inputs outputs; };

      packages = forEachSystem (pkgs: import ./pkgs { inherit pkgs; });
      # devShells = forEachSystem (pkgs: import ./shell.nix { inherit pkgs; });
      formatter = forEachSystem (pkgs: pkgs.nixpkgs-fmt);

      nixosConfigurations = {

        <<ion-system-config>>
        <<nixos2-system-config>>
        <<vezzini-system-config>>

      };

      # Standalone home-manager configuration entrypoint
      # Available through 'home-manager --flake .#your-username@your-hostname'
      homeConfigurations = {

        "stratos@nixos2" = lib.homeManagerConfiguration {
          modules = [
            # > Our main home-manager configuration file <
            ./home/stratos/nixos2.nix
          ];
          pkgs = nixpkgs.legacyPackages.x86_64-linux; # Home-manager requires 'pkgs' instance
          extraSpecialArgs = { inherit inputs outputs; };
        };

      };
    };
}
```


## Hosts


### ion

``` {.nix #ion-system-config}
ion = nixpkgs.lib.nixosSystem {
  modules = [
    disko.nixosModules.disko
    ./hosts/ion/hardware-configuration.nix
    ./hosts/ion/configuration.nix
    ./hosts/ion/additional.nix
  ];
  specialArgs = { inherit inputs outputs; };
};
```

### nixos2

``` {.nix #nixos2-system-config}
nixos2 = nixpkgs.lib.nixosSystem {
  modules = [
    disko.nixosModules.disko
    ./hosts/nixos2/hardware-configuration.nix
    ./hosts/nixos2/configuration.nix
    ./hosts/nixos2/additional.nix
  ];
  specialArgs = { inherit inputs outputs; };
};
```

### vezzini

``` {.nix #vezzini-system-config}
vezzini = nixpkgs.lib.nixosSystem {
  modules = [
    disko.nixosModules.disko
    ./hosts/vezzini/hardware-configuration.nix
    ./hosts/vezzini/configuration.nix
    ./hosts/vezzini/additional.nix
  ];
  specialArgs = { inherit inputs outputs; };
};
```
