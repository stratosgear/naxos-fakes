# Users

Here we define a list of users that we might need installed in our hosts.  Use the
definition name (user-someUserName, within <<) to include the user in the host


## stratos

``` {.nix file=nix/system/users/stratos.nix}
{ pkgs, config, ... }:
let ifTheyExist = groups: builtins.filter (group: builtins.hasAttr group config.users.groups) groups;
in
{
  programs.zsh.enable = true;
  users.mutableUsers = false;
  users.users = {
    test = {
      isNormalUser = true;
      # Generated with: mkpasswd -m sha-512
      hashedPassword = "$6$iHZ.Np.AumC5j4wR$ucOJJT/N/opMg.Aw4xbGy7fUvwTI.15lfbwmIoMKLh4M3GdrR6Ode3ffBnr8lqoaTGHPWn2jD4jBjp/Uq7xPw/";
      # hashedPasswordFile = config.sops.secrets.test-password.path;
      extraGroups = [
        "wheel"
      ];
    };
    stratos = {
      isNormalUser = true;
      shell = pkgs.zsh;
      extraGroups = [
        "wheel"
      #   "video"
      #   "audio"
        "networkmanager"
      # ] ++ ifTheyExist [
      #   "network"
      #   "i2c"
      #   "docker"
      #   "git"
      #   "libvirtd"
      ];
      # hashedPasswordFile = "/persist/passwords/stratos";
      hashedPasswordFile = config.sops.secrets.stratos-password.path;

      openssh.authorizedKeys.keys = [ (builtins.readFile ./stratos-ssh-key.pub) ];
      packages = [ pkgs.home-manager ];
    };
  };

  sops.secrets.stratos-password = {
    sopsFile = ../secrets.yaml;
    neededForUsers = true;
  };
  sops.secrets.test-password = {
    sopsFile = ../secrets.yaml;
    neededForUsers = true;
  };

  #  home-manager.users.stratos = import ../../../../home/stratos/${config.networking.hostName}.nix;

}
```

### ssh key

You have to manually create an ssh key at:

- nix/keys/stratos-ssh-key.pub
