# Common Packages

These are some packages that are installed in **all** the hosts, due to the fact that
they are so core/basic.


``` {.nix file=nix/system/global.nix}
{ inputs, outputs, pkgs, lib, ... }: {

  imports = [
    inputs.home-manager.nixosModules.home-manager
    ./sops.nix
    ./nix.nix
    # ./optin-persistence.nix
    # ./tailscale.nix
  ] ++ (builtins.attrValues outputs.nixosModules);

  home-manager.extraSpecialArgs = { inherit inputs outputs; };

  # Basic global installation of programs that do not need additional configuration
  environment.systemPackages = [
    pkgs.sops
    pkgs.nixpkgs-fmt
  ];

  <<global-i18n>>

  nixpkgs = {
    overlays = builtins.attrValues outputs.overlays;
    config = {
      allowUnfree = true;
    };
  };

  # Increase open file limit for sudoers
  security.pam.loginLimits = [
    {
      domain = "@wheel";
      item = "nofile";
      type = "soft";
      value = "524288";
    }
    {
      domain = "@wheel";
      item = "nofile";
      type = "hard";
      value = "1048576";
    }
  ];

  <<global-openssh>>

}
```

## nix

Some initial nix configuration

``` {.nix file=nix/packages/nix.nix}
{ inputs, lib, ... }:
{
  nix = {
    settings = {
      trusted-users = [ "root" "@wheel" ];
      auto-optimise-store = lib.mkDefault true;
      experimental-features = [ "nix-command" "flakes" "repl-flake" ];
      warn-dirty = false;
      system-features = [ "kvm" "big-parallel" "nixos-test" ];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      # Delete older generations too
      options = "--delete-older-than 2d";
    };

    # Add each flake input as a registry
    # To make nix3 commands consistent with the flake
    registry = lib.mapAttrs (_: value: { flake = value; }) inputs;

    # Add nixpkgs input to NIX_PATH
    # This lets nix2 commands still use <nixpkgs>
    nixPath = [ "nixpkgs=${inputs.nixpkgs.outPath}" ];
  };
}
```
