# Packages

## Openssh

``` {.nix #global-openssh}
let
  inherit (config.networking) hostName;
  hosts = outputs.nixosConfigurations;
  # TODO: This prepopulates some known ssh hosts. But turning it  off for now
  # pubKey = host: ../../${host}/ssh_host_ed25519_key.pub;

  # Sops needs access to the keys before the persist dirs are even mounted; so
  # just persisting the keys won't work, we must point at /persist
  # hasOptinPersistence = config.environment.persistence ? "/persist";
in
{
  services.openssh = {
    enable = true;
    settings = {
      # Harden
      PasswordAuthentication = true;
      # TODO: Turn this OFF after debugging
      PermitRootLogin = "yes";
      # Automatically remove stale sockets
      StreamLocalBindUnlink = "yes";
      # Allow forwarding ports to everywhere
      GatewayPorts = "clientspecified";
    };

    # hostKeys = [{
    #   path = "${lib.optionalString hasOptinPersistence "/persist"}/etc/ssh/ssh_host_ed25519_key";
    #   type = "ed25519";
    # }];
  };

  programs.ssh = {
    # Each hosts public key
    # TODO: This prepopulates some known ssh hosts. But turning it  off for now
    # knownHosts = builtins.mapAttrs
    #   (name: _: {
    #     publicKeyFile = pubKey name;
    #     extraHostNames =
    #       (lib.optional (name == hostName) "localhost");
    #     #   (lib.optional (name == hostName) "localhost") ++ # Alias for localhost if it's the same host
    #     #   (lib.optionals (name == gitHost) [ "m7.rs" "git.m7.rs" ]); # Alias for m7.rs and git.m7.rs if it's the git host
    #   })
    #   hosts;
  };

  # Passwordless sudo when SSH'ing with keys
  security.pam.enableSSHAgentAuth = true;
}
```
