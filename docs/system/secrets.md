# Secrets

Secrets are handled through sops-nix

!!! warning "TODO"

    Describe the whole workflow!


``` {.nix file=nix/packages/sops.nix}
{ inputs, lib, config, ... }:
let
  isEd25519 = k: k.type == "ed25519";
  getKeyPath = k: k.path;
  keys = builtins.filter isEd25519 config.services.openssh.hostKeys;
in
{
  imports = [
    inputs.sops-nix.nixosModules.sops
  ];

  sops = {
    age.sshKeyPaths = map getKeyPath keys;
  };

  # sops.age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
  # sops.age.sshKeyPaths = [];
  # sops.gnupg.sshKeyPaths = [];
}
```

## Secrets file

This is the actual file that contains the secrets.  You see it here in encrypted form.

``` {.yaml file=nix/system/secrets.yaml}
test-password: ENC[AES256_GCM,data:hrdWzUhCp8ENwE5tLUojNdumE8pfsrmxfiDIOZnuGixwdtPelFK5qJSxVWWPtb/LzTfi5ZmGeNh/twiB6Fd/+faLzysjksKYiw==,iv:PN7Ou0yB0aWcTOfuxNbi8zDiio01nfqqzUyWArjmjjw=,tag:3r2gDdaMpPknoXFPdLKwcA==,type:str]
stratos-password: ENC[AES256_GCM,data:Yz1waoklhwXICN0DaQQ6j74kLcAH7z1d2UbB3p4DaXrPzbicfgwk8/M6fx04OBe0XZg+7DxqpnjutsFHcY/CF9V2R7bNEFwbpQ==,iv:OxotTMqJecY/boN6q/qqtbaijbuTFbpi2/2eddGAkKQ=,tag:DHmxOcalqk6FngWpKOGexQ==,type:str]
sops:
    kms: []
    gcp_kms: []
    azure_kv: []
    hc_vault: []
    age:
        - recipient: age1m2zk8strrpxrjzfpfh9a8yd63xnyz37uh82sc0ynfawjtp478e6s5xqurx
          enc: |
            -----BEGIN AGE ENCRYPTED FILE-----
            YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSAwT3BXZlRLVFJ5dHNjcWRa
            K1ZEeG93cTVaMFByRmM5WDlSTFozTm1iSjFzCjdVWThmUFF2enZkaXZpU01vbnV6
            RzZERERFY0hveFEyUkkvcHczN0UyNGMKLS0tIDdMakhnTXBQeWZSTDhjbTNwcGpz
            Qk9ZV0xGT2xrbk9IT21Xb0JCVVQzbzgKYs7gYsa4ZSDQjFhsq18V4M1J695/muHX
            bgckaAiQBzK4blbtX9xSNyvhyacppVCITpoR9++YhaGzbHNJKP7FXA==
            -----END AGE ENCRYPTED FILE-----
        - recipient: age1z7v3gd6sa5dkdvassy8yda6gulll8kvxzpksjy7kj3qxjfzqeqjqwdxaem
          enc: |
            -----BEGIN AGE ENCRYPTED FILE-----
            YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSBkU3NJZWZ4Sjh5RUl0elFG
            eEZNcmpvUVNHTFpPdjNCQitjZUV1Qkg0SEc4CjR0OG9PcFpBSVBhUTFYSmN3cTJq
            T1BNd2hXODJHMHNnVERPSkpCWitKc2MKLS0tIEVLcCt1UWhkVWcwcWVZY20wUnJU
            ME80UTdoWnArR2NWcXFQdVhHZEdLRjgKTpdS24HunUeRWFnpVfgWJRqGdm+pGo1V
            Ir+MKc9gzDT/FDmuTTUQ6vIlU+tDYLwXKlEDz3+qoQHjRV+PoP3yBA==
            -----END AGE ENCRYPTED FILE-----
        - recipient: age18gvanesrrxpf4zjrjyjp8and5p5cng9jstzgksxmq776ulwh9geq2yyc68
          enc: |
            -----BEGIN AGE ENCRYPTED FILE-----
            YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSBnMTBqTVo3V051a1cxUytH
            OXo1cGh2ampPQmVKdlNnVzg2SWpTNm85SWxVCkxGYjNLT0hDU3FmSkgrNStwWUpz
            TjVsOUFXQzM5ZnVqWjZZdGpCQmpubTgKLS0tIFRNc0RyNS8vSC90ZUNqdi8yTEhv
            cURhcnVXMlp1UlFseStmcHhhRThHQmMKjp4jozAONW6jIsP7KKC767frSgW6Q2c4
            ClhWVL/1klhnDIPHDi0Q1mk95nl4wx+ncfd7MBdzAFCvb3739OIkVA==
            -----END AGE ENCRYPTED FILE-----
    lastmodified: "2023-11-19T13:56:38Z"
    mac: ENC[AES256_GCM,data:MKUjnaWXPVR5tttpmuj6vktIFHprmjK3YGbFQdlKzx96/Mu8WmxVJk5P+PQkRVSkT5uRClOJogweAFqirUUZXbaTaNmrLYoj5RxVNV/nOGnsQ9kM0Z2bU/l+fXpAAwwP38nq28XSnTWkVnjGyBJ9bFiSwbi/G3lnPsk03ex8M8o=,iv:fDmaZgUErSlkolNiOphEtgaZ8usL63I2Eqx+r6Fg4mU=,tag:oizg9jFQU4wg3Yle1nDcNg==,type:str]
    pgp: []
    unencrypted_suffix: _unencrypted
    version: 3.8.1
```

# Sops configuration file

This is the file that configures sops.  It allows a certain number of users or systems
to decrypt and read the secret file above.

``` {.yaml file=nix/.sops.yaml}
keys:
  - &user_stratos age1m2zk8strrpxrjzfpfh9a8yd63xnyz37uh82sc0ynfawjtp478e6s5xqurx
  - &system_nixos2 age1z7v3gd6sa5dkdvassy8yda6gulll8kvxzpksjy7kj3qxjfzqeqjqwdxaem
  - &system_vezzini age18gvanesrrxpf4zjrjyjp8and5p5cng9jstzgksxmq776ulwh9geq2yyc68

creation_rules:
  - path_regex: system/secrets.ya?ml$
    key_groups:
    - age:
      - *user_stratos
      - *system_nixos2
      - *system_vezzini
```
