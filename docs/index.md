# What is this?

This an attempt the maintain and at the same time document my dotfiles.

In a fit of mental derangement I decided to both go **all** [Nixos][https://nixos.org]
on this and at the **same time** try to do it following a [literal
programming][https://en.wikipedia.org/wiki/Literate_programming] paradigm.


!!! note annotate "Watch for the landmines"

    As I am a complete newbie to both Nixos and literal programming, please beware of any
    accidental landmines that might lie around.


## What's up with the name?

Tweaking Nixos to make it Naxos (being Greek, helps with this) and changing Flakes to
Fakes (being a fake myself, see note above) was an obvious project name for this repo!

