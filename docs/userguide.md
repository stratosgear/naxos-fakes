# How to use

The information contained in this repo (and in it's published form online) is mainly for
my own use as a documentation for the installation and maintenance of the systems that I
am administering.

Since I have used a lot of similar repos online as a reference, it is only fair if I
publicly publish my own repo too.  Hopefully, one day, it might be of good enough
quality to be used as a reference too. 😄

This repo is used for two main tasks:

- Bootstraping new hosts
- Customizing existing hosts


## Bootstraping new hosts

A host means a new laptop, desktop, server (vm?)

Bootstraping it means to put a very basic bootable Nixos installation on it.

Detailed instructions on bootstrapping appears in the relevant page. In summary it is
triggered with a:

`nix run github:nix-community/nixos-anywhere -- --flake .#[hostname] [hostname]`

## Customizing existing hosts

Once an OS installation is performed you can proceed with further customizations, like
installing and configuring programs, services etc

Again, further instructions for customization is available at the Hosts Costumization
pages.  In summary they are triggered with:

`sudo nixos-rebuild switch --flake .#[hostname]`

for systemwide updates, or with:

`home-manager switch --flake .#[username]@[hostname]`

for user settings

